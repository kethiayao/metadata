import unittest

from d3m_metadata import params


class TestParams(unittest.TestCase):
    def test_params(self):
        class TestParams(params.Params):
            a: str
            b: int

        test_params = TestParams({'a': 'foo', 'b': 42})

        self.assertEqual(test_params['a'], 'foo')
        self.assertEqual(test_params['b'], 42)

        with self.assertRaisesRegex(ValueError, 'Not all parameters are specified'):
            TestParams({'a': 'foo'})

        with self.assertRaisesRegex(ValueError, 'Additional parameters are specified'):
            TestParams({'a': 'foo', 'b': 42, 'c': None})

        test_params = TestParams(a='bar', b=10)
        self.assertEqual(test_params['a'], 'bar')
        self.assertEqual(test_params['b'], 10)

        with self.assertRaisesRegex(TypeError, 'Value \'.*\' is not an instance of the type'):
            TestParams({'a': 'foo', 'b': 10.1})

        with self.assertRaisesRegex(TypeError, 'Only methods and attribute type annotations can be defined on Params class'):
            class ErrorParams(params.Params):
                a = str
                b = int


if __name__ == '__main__':
    unittest.main()
